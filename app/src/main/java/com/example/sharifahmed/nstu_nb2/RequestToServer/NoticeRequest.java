package com.example.sharifahmed.nstu_nb2.RequestToServer;

import java.util.ArrayList;

/**
 * Created by SHARIF AHMED on 11/28/2016.
 */

public class NoticeRequest {
    private String noticeTittle;
    private String noticeCategory;
    private String noticeDepartment;
    private String noticeDate;
    private String noticeTime;

    public NoticeRequest() {
    }

    public String getNoticeTittle() {
        return noticeTittle;
    }

    public void setNoticeTittle(String noticeTittle) {
        this.noticeTittle = noticeTittle;
    }

    public String getNoticeCategory() {
        return noticeCategory;
    }

    public void setNoticeCategory(String noticeCategory) {
        this.noticeCategory = noticeCategory;
    }

    public String getNoticeDepartment() {
        return noticeDepartment;
    }

    public void setNoticeDepartment(String noticeDepartment) {
        this.noticeDepartment = noticeDepartment;
    }

    public String getNoticeDate() {
        return noticeDate;
    }

    public void setNoticeDate(String noticeDate) {
        this.noticeDate = noticeDate;
    }

    public String getNoticeTime() {
        return noticeTime;
    }

    public void setNoticeTime(String noticeTime) {
        this.noticeTime = noticeTime;
    }

}
