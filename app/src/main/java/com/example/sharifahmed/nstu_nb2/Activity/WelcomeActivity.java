package com.example.sharifahmed.nstu_nb2.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharifahmed.nstu_nb2.R;
import com.example.sharifahmed.nstu_nb2.ShPreferrence.ShareDPreferrence;


public class WelcomeActivity extends AppCompatActivity {

    private TextView welcomeText;
    private ShareDPreferrence shareDPreferrence;
    private  boolean isUserClickedButton = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        shareDPreferrence = new ShareDPreferrence(this);

        welcomeText = (TextView) findViewById(R.id.welcomeTextid);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setTitle(" NSTU Notice Board");
        actionBar.setIcon(R.drawable.nstulogo);

        welcomeText.setText("Welcome To NSTU Notice Board as "+shareDPreferrence.RetrieveMethod().getCatName());


    }
//**************************** For Menu Item Work ***************************************************8
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = new MenuInflater(WelcomeActivity.this);
        menuInflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){

            case R.id.logInId :
                Toast.makeText(this, "Clicked Log In Menu", Toast.LENGTH_SHORT).show();
                break;

            case R.id.logOutId :

                if(shareDPreferrence.deleteSharedData()) {
                    Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                break;


            case R.id.changeEmailId :
                Toast.makeText(this, "Clicked Change Email Menu", Toast.LENGTH_SHORT).show();
                break;

            case R.id.changePasswordId :
                Toast.makeText(this, "Clicked Change Password Menu", Toast.LENGTH_SHORT).show();
                break;

            case R.id.adminId :
                Toast.makeText(this, "Clicked Admin Menu", Toast.LENGTH_SHORT).show();
                break;


        }
        return super.onOptionsItemSelected(item);
    }
//**********************************End Menu Item Work**********************************************


    @Override
    public void onBackPressed() {

        if(!isUserClickedButton){
            Toast.makeText(this, "Press Back again to exit", Toast.LENGTH_SHORT).show();
            isUserClickedButton = true;
        }else {
            super.onBackPressed();
        }
        new CountDownTimer(3000,1000){

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                isUserClickedButton = false;
            }
        }.start();
    }
}
