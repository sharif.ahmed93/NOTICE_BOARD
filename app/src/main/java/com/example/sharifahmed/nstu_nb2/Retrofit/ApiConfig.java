package com.example.sharifahmed.nstu_nb2.Retrofit;

import com.example.sharifahmed.nstu_nb2.ResponseFromServer.ServerResponse;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by Mushtaq on 03-06-2016.
 */
public interface ApiConfig {


    @Multipart
    @POST("NSTU_NOTICE_BOARD/upload.php")

    Call<ServerResponse> upload(

            @Header("Authorization") String authorization,

            @PartMap Map<String, RequestBody> map,

            @Part("notice_title") RequestBody noticeTitle,

            @Part("notice_category") RequestBody noticeCat


    );
}
