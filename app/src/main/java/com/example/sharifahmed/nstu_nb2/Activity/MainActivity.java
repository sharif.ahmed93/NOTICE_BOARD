package com.example.sharifahmed.nstu_nb2.Activity;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;


import com.example.sharifahmed.nstu_nb2.R;
import com.example.sharifahmed.nstu_nb2.ShPreferrence.ShareDPreferrence;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Spinner spinnerCategory;
    private Spinner spinnerDepartment;

    RadioGroup  radioAdminGroup;
    RadioButton radioAdminButton;


    private LinearLayout linearLayoutClarkeAdmin;

    private ArrayList<String> categoryList;
    private ArrayList<String> departmentList;
    private ArrayAdapter<String>arrayAdapterCategory;
    private ArrayAdapter<String>arrayAdapterDepartment;

    private ShareDPreferrence shareDPreferrence;

    private String slectCategoryItem;
    private String selectDepartmentItem;
    private String clarkAdmin;

    private AlertDialog alertDialog;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioAdminGroup = (RadioGroup) findViewById(R.id.clarkeRadioGroupId);

        clarkAdmin="";


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setTitle(" NSTU Notice Board");
        actionBar.setIcon(R.drawable.nstulogo);


        linearLayoutClarkeAdmin = (LinearLayout) findViewById(R.id.clarkeAdminLinearLayoutId);

        shareDPreferrence = new ShareDPreferrence(this);
        if(shareDPreferrence.getShareDPreferrenceStatus()==true && shareDPreferrence.RetrieveMethod().getCatName().equals("Teacher")){
            Toast.makeText(MainActivity.this, "You Are Registered as "+shareDPreferrence.RetrieveMethod().getCatName(), Toast.LENGTH_SHORT).show();
            Intent goWelcomeIntent = new Intent(MainActivity.this,TeachersNoticeActivity.class);
            finish();
            startActivity(goWelcomeIntent);
        }else if (shareDPreferrence.getShareDPreferrenceStatus()==true && shareDPreferrence.RetrieveMethod().getCatName().equals("Clarke")){
            Toast.makeText(MainActivity.this, "You Are Registered as "+shareDPreferrence.RetrieveMethod().getCatName()+" "+shareDPreferrence.RetrieveMethod().getDeptName(), Toast.LENGTH_SHORT).show();
            Intent goWelcomeIntent = new Intent(MainActivity.this,ClarkeNoticeActivity.class);
            finish();
            startActivity(goWelcomeIntent);
        }else if (shareDPreferrence.getShareDPreferrenceStatus()==true && shareDPreferrence.RetrieveMethod().getCatName().equals("Student")){
            Toast.makeText(MainActivity.this, "You Are Registered as "+shareDPreferrence.RetrieveMethod().getCatName(), Toast.LENGTH_SHORT).show();
            Intent goWelcomeIntent = new Intent(MainActivity.this,NoticeStudents.class);
            finish();
            startActivity(goWelcomeIntent);
        }


        spinnerCategory = (Spinner) findViewById(R.id.spinnerCategory);
        spinnerDepartment = (Spinner) findViewById(R.id.spinnerDepartment);

        categoryList = new ArrayList<>();
        categoryList.add("Category");
        categoryList.add("Teacher");
        categoryList.add("Clarke");
        categoryList.add("Student");

        departmentList = new ArrayList<>();
        departmentList.add("Department");
        departmentList.add("CSTE");
        departmentList.add("FIMS");
        departmentList.add("PHARMACY");
        departmentList.add("ACCE");
        departmentList.add("FTNS");
        departmentList.add("ESDM");
        departmentList.add("Agriculture");
        departmentList.add("ICE");
        departmentList.add("EEE");
        departmentList.add("BBA");
        departmentList.add("IIT");
        departmentList.add("ESHS");


        arrayAdapterCategory = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,categoryList);
        arrayAdapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        arrayAdapterDepartment = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,departmentList);
        arrayAdapterDepartment.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerCategory.setAdapter(arrayAdapterCategory);
        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                slectCategoryItem = spinnerCategory.getSelectedItem().toString();
                if(slectCategoryItem.equals("Clarke")){
                    linearLayoutClarkeAdmin.setVisibility(View.VISIBLE);
                }
                else{
                    linearLayoutClarkeAdmin.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerDepartment.setAdapter(arrayAdapterDepartment);
        spinnerDepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectDepartmentItem = spinnerDepartment.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }
    public void clickSubmit(View view) {

        int id = radioAdminGroup.getCheckedRadioButtonId();

        if (id!=-1){
            radioAdminButton = (RadioButton) radioAdminGroup.findViewById(id);
            clarkAdmin = (String) radioAdminButton.getText();
        }

        if(slectCategoryItem.equals("Category") || selectDepartmentItem.equals("Department")){
            Toast.makeText(this, "Select Item First", Toast.LENGTH_SHORT).show();
        }
        else if (slectCategoryItem.equals("Clarke")){

            if(!clarkAdmin.equals("")){

                Toast.makeText(this,clarkAdmin, Toast.LENGTH_SHORT).show();
                shareDPreferrence.SaveMethod(slectCategoryItem,selectDepartmentItem,clarkAdmin);
                registered();
            }
            else {
                Toast.makeText(this,"Select Admin", Toast.LENGTH_SHORT).show();
            }
        }
        else{

            if(slectCategoryItem.equals("Teacher") || slectCategoryItem.equals("Student")){

                shareDPreferrence.SaveMethod(slectCategoryItem,selectDepartmentItem);
                registered();
            }

        }
    }

    public void registered(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are You Sure , You Wanted To Category as "+slectCategoryItem+" And Department as "+selectDepartmentItem);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, "Clicked Yes button", Toast.LENGTH_SHORT).show();

                ShareDPreferrence.SavedClass savedClass = new ShareDPreferrence.SavedClass();
                savedClass = shareDPreferrence.RetrieveMethod();

                switch (savedClass.getCatName()){
                    case "Teacher" :
                        Intent intentTeacher = new Intent(MainActivity.this,TeachersNoticeActivity.class);
                        finish();
                        startActivity(intentTeacher);
                        break;
                    case "Clarke" :
                        Intent intentClarke = new Intent(MainActivity.this,ClarkeNoticeActivity.class);
                        finish();
                        startActivity(intentClarke);
                        break;
                    case "Student" :
                        Intent intentStudent = new Intent(MainActivity.this,NoticeStudents.class);
                        finish();
                        startActivity(intentStudent);
                        break;
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

//***********************************End Submit Button Work*****************************************************
}
