package com.example.sharifahmed.nstu_nb2.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.sharifahmed.nstu_nb2.R;
import com.example.sharifahmed.nstu_nb2.ShPreferrence.ShareDPreferrence;


public class ClarkeLoginActivity extends AppCompatActivity {

    Button logInBtn;
    ShareDPreferrence shareDPreferrence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clarke_login);
        shareDPreferrence = new ShareDPreferrence(this);


        logInBtn = (Button) findViewById(R.id.clarkLoginButtonlId);
        logInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClarkeLoginActivity.this, ClarkeAfterLoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
