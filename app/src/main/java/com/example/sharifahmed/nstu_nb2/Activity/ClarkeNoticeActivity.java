package com.example.sharifahmed.nstu_nb2.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharifahmed.nstu_nb2.R;
import com.example.sharifahmed.nstu_nb2.ShPreferrence.ShareDPreferrence;


public class ClarkeNoticeActivity extends AppCompatActivity {


    ShareDPreferrence shareDPreferrence;

    TextView clarkeText ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clarke_notice);
        clarkeText  = (TextView) findViewById(R.id.noticeText);

        /*clarkeText.setText(shareDPreferrence.RetrieveMethod().getClarkAdmin());*/

        shareDPreferrence = new ShareDPreferrence(this);
    }




    //**************************** For Menu Item Work ***************************************************8
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = new MenuInflater(ClarkeNoticeActivity.this);
        menuInflater.inflate(R.menu.menu_without_login_clarke,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(!shareDPreferrence.RetrieveMethod().getClarkAdmin().equals("Yes")){
            item.setVisible(false);
        }



        else {
            switch(item.getItemId()){

                case R.id.logInId :
                    Toast.makeText(this, "Clicked Log In Menu", Toast.LENGTH_SHORT).show();
                    Intent goLoginIntent = new Intent(ClarkeNoticeActivity.this,ClarkeLoginActivity.class);
                    finish();
                    startActivity(goLoginIntent);
                    break;

            }
        }
        return super.onOptionsItemSelected(item);

    }
//**********************************End Menu Item Work**********************************************

}
