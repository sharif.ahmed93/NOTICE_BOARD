package com.example.sharifahmed.nstu_nb2.ResponseFromServer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SHARIF AHMED on 12/15/2016.
 */
public class NoticeResponse {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("notice_title")
    @Expose
    private String noticeTitle;
    @SerializedName("notice_category")
    @Expose
    private String noticeCategory;
    @SerializedName("file")
    @Expose
    private String file;

    /**
     *
     * @return
     * The id
     */

    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */


    public void setId(String id) {
        this.id = id;
    }



    /**
     *
     * @return
     * The title
     */

    public String getTitle() {
        return noticeTitle;
    }

    /**
     *
     * @param noticeTitle
     * The title
     */

    public void setTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }


    /**
     *
     * @return
     * The category
     */

    public String getCategory() {
        return noticeCategory;
    }

    /**
     *
     * @param noticeCategory
     * The category
     */

    public void setCategory(String noticeCategory) {
        this.noticeCategory = noticeCategory;
    }


    /**
     *
     * @return
     * The file
     */

    public String getFile() {
        return file;
    }


    /**
     *
     * @param file
     * The file
     */

    public void setFile(String file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return noticeTitle                                                                                                                                                                                                                                                                                                                   ;
    }
}
