package com.example.sharifahmed.nstu_nb2.Retrofit;

import com.example.sharifahmed.nstu_nb2.ResponseFromServer.NoticeResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by SHARIF AHMED on 12/15/2016.
 */
public interface StudentsNoticeApi {
    @GET("NSTU_NOTICE_BOARD/students.php")
    Call<List<NoticeResponse>> getAllStudentsNoticeData();
}
