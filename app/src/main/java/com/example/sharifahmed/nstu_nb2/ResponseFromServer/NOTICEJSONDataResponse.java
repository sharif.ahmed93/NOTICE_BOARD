package com.example.sharifahmed.nstu_nb2.ResponseFromServer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SHARIF AHMED on 11/4/2016.
 */
public class NOTICEJSONDataResponse {

/*
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("notice_title")
    @Expose
    private String noticeTitle;//full_name
    @SerializedName("notice_category")
    @Expose
    private String noticeCategory;//user_name
    @SerializedName("file")
    @Expose
    private String file;//phone

    @SerializedName("date")
    @Expose
    private String date;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNoticeTitle() {
        return noticeTitle;
    }

    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    public String getNoticeCategory() {
        return noticeCategory;
    }

    public void setNoticeCategory(String noticeCategory) {
        this.noticeCategory = noticeCategory;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


*/


    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("notice_title")
    @Expose
    private String noticeTitle;
    @SerializedName("notice_category")
    @Expose
    private String noticeCategory;
    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("date")
    @Expose
    private String date;


    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The fullName
     */
    public String getNoticeTitle() {
        return noticeTitle;
    }
    /**
     *
     * @param noticeTitle
     * The full_name
     */
    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    /**
     *
     * @return
     * The userName
     */
    public String getNoticeCategory() {
        return noticeCategory;
    }

    /**
     *
     * @param noticeCategory
     * The user_name
     */
    public void setNoticeCategory(String noticeCategory) {
        this.noticeCategory = noticeCategory;
    }

    /**
     *
     * @return
     * The phoneNumber
     */
    public String getFile() {
        return file;
    }

    /**
     *
     * @param file
     * The phone_number
     */
    public void setFile(String file) {
        this.file = file;
    }

    /**
     *
     * @return
     * The email
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The email
     */
    public void setDate(String date) {
        this.date = date;
    }

}
