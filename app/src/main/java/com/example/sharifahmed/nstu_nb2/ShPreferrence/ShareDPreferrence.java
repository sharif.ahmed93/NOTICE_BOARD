package com.example.sharifahmed.nstu_nb2.ShPreferrence;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by SHARIF AHMED on 11/25/2016.
 */

public class ShareDPreferrence {

    private Context context;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private final static String CAT_KEY="category_name";
    private final static String DEPT_KEY="department";
    private final static String ADMIN_KEY="admin";

    private final static String CLARKE_IDENTITY_KEY="clarke_identity";
    private final static String CLARKE_PASS_KEY="clarke_password";


    public ShareDPreferrence(Context context){
        this.context=context;
        sharedPreferences = context.getSharedPreferences("MyPref",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    // This Method For Registered As Teacher or Student or Clarke Category Saved in SharedPreferrence
    public void SaveMethod(String cat,String dept){
        editor.putString(CAT_KEY,cat);
        editor.putString(DEPT_KEY,dept);
        editor.commit();
    }
    public void SaveMethod(String cat,String dept,String admin){
        editor.putString(CAT_KEY,cat);
        editor.putString(DEPT_KEY,dept);
        editor.putString(ADMIN_KEY,admin);
        editor.commit();
    }

    // This Method For Clarke LOGIN data Saved in SharedPreferrence
    public void setClarkeLoginData(String clarkeIdentity,String clarkePass){
        editor.putString(CLARKE_IDENTITY_KEY,clarkeIdentity);
        editor.putString(CLARKE_PASS_KEY,clarkePass);
        editor.commit();
    }

    //This Method For Retrieving AlL Sharedpreferrence SAVED data by using SavedClass
    public SavedClass RetrieveMethod(){

        SavedClass savedClass = new SavedClass();

        String catDataToBeShow = sharedPreferences.getString(CAT_KEY,"Data Not Found");
        String deptDataToBeShow = sharedPreferences.getString(DEPT_KEY,"Data Not Found");
        String clarkAdminDataToBeShow = sharedPreferences.getString(ADMIN_KEY,"Data Not Found");

        String clarkeIdentityDataToBeShow = sharedPreferences.getString(CLARKE_IDENTITY_KEY,"Data Not Found");
        String clarkePasswordDataToBeShow = sharedPreferences.getString(CLARKE_PASS_KEY,"Data Not Found");


        savedClass.setCatName(catDataToBeShow);
        savedClass.setDeptName(deptDataToBeShow);
        savedClass.setClarkAdmin(clarkAdminDataToBeShow);

        savedClass.setClarkeIdentity(clarkeIdentityDataToBeShow);
        savedClass.setClarkePassword(clarkePasswordDataToBeShow);

        return savedClass;

    }
//This Method For CategoryRegistered Status that you are slect any item of category
    public boolean getShareDPreferrenceStatus() {
        String categoryName = sharedPreferences.getString(CAT_KEY,null);
        String departmentName = sharedPreferences.getString(DEPT_KEY,null);
        if(categoryName!=null && departmentName!=null)
            return true;
        else
            return false;
    }
    //This Method For ClarkeLogin Status
    public boolean getClarkeLoginStatus() {
        String clarkeIdentity = sharedPreferences.getString(CLARKE_IDENTITY_KEY,null);
        String clarkePassword = sharedPreferences.getString(CLARKE_PASS_KEY,null);
        if(clarkeIdentity!=null && clarkePassword!=null)
            return true;
        else
            return false;
    }

    public boolean deleteSharedData(){
        editor.clear();
        editor.commit();
        return true;
    }



    public static class SavedClass{

        private   String catName;
        private   String deptName;
        private   String clarkAdmin;
        private   String clarkeIdentity;
        private   String clarkePassword;

        public String getClarkAdmin() {
            return clarkAdmin;
        }

        public void setClarkAdmin(String clarkAdmin) {
            this.clarkAdmin = clarkAdmin;
        }

        public String getClarkeIdentity() {
            return clarkeIdentity;
        }

        public void setClarkeIdentity(String clarkeIdentity) {
            this.clarkeIdentity = clarkeIdentity;
        }

        public String getClarkePassword() {
            return clarkePassword;
        }

        public void setClarkePassword(String clarkePassword) {
            this.clarkePassword = clarkePassword;
        }

        public SavedClass() {
        }

        public  String getDeptName() {
            return deptName;
        }

        private   void setDeptName(String deptName) {
            this.deptName = deptName;
        }

        public  String getCatName() {
            return catName;
        }

        private   void setCatName(String catName) {
            this.catName = catName;
        }



    }




}
