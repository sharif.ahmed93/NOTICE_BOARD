package com.example.sharifahmed.nstu_nb2.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sharifahmed.nstu_nb2.R;
import com.example.sharifahmed.nstu_nb2.ResponseFromServer.NOTICEJSONDataResponse;
import com.squareup.picasso.Picasso;


import java.util.List;

public class NoticeListAdapter extends ArrayAdapter<NOTICEJSONDataResponse> {

    List<NOTICEJSONDataResponse> contactList;
    Context context;
    private LayoutInflater mInflater;

    private static String LOAD_BASE_URL = "http://192.168.43.101/NSTU_NOTICE_BOARD/";
    // Constructors
    public NoticeListAdapter(Context context, List<NOTICEJSONDataResponse> objects) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        contactList = objects;
    }

    @Override
    public NOTICEJSONDataResponse getItem(int position) {
        return contactList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.layout_row_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        NOTICEJSONDataResponse item = getItem(position);

        vh.textViewName.setText(item.getNoticeTitle());
        vh.textViewEmail.setText(item.getNoticeCategory());
        Picasso.with(context).load(LOAD_BASE_URL+item.getFile()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(vh.imageView);

        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final ImageView imageView;
        public final TextView textViewName;
        public final TextView textViewEmail;

        private ViewHolder(RelativeLayout rootView, ImageView imageView, TextView textViewName, TextView textViewEmail) {
            this.rootView = rootView;
            this.imageView = imageView;
            this.textViewName = textViewName;
            this.textViewEmail = textViewEmail;
        }

        public static ViewHolder create(RelativeLayout rootView) {
            ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);
            TextView textViewName = (TextView) rootView.findViewById(R.id.textViewName);
            TextView textViewEmail = (TextView) rootView.findViewById(R.id.textViewEmail);
            return new ViewHolder(rootView, imageView, textViewName, textViewEmail);
        }
    }
}
