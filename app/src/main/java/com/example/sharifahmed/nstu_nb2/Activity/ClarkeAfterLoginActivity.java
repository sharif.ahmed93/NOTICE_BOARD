package com.example.sharifahmed.nstu_nb2.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.sharifahmed.nstu_nb2.R;
import com.example.sharifahmed.nstu_nb2.ShPreferrence.ShareDPreferrence;


public class ClarkeAfterLoginActivity extends AppCompatActivity {


    ShareDPreferrence shareDPreferrence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clarke_after_login);
        shareDPreferrence = new ShareDPreferrence(this);

    }


    //**************************** For Menu Item Work ***************************************************8
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = new MenuInflater(ClarkeAfterLoginActivity.this);
        menuInflater.inflate(R.menu.menu_with_login_clarke,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){


            case R.id.takeNoticePictureId :
                Toast.makeText(this, "Clicked Take Notice Picture Menu", Toast.LENGTH_SHORT).show();
                break;
            case R.id.uploadNoticePictureId :
                Toast.makeText(this, "Clicked Upload Notice Picture Menu", Toast.LENGTH_SHORT).show();
                break;
            case R.id.uploadNoticeFileId :
                Toast.makeText(this, "Clicked Upload Notice File Menu", Toast.LENGTH_SHORT).show();
                Intent gouploadintent = new Intent(ClarkeAfterLoginActivity.this, UploadNoticeActivity.class);
                startActivity(gouploadintent);
                break;

            case R.id.logOutId :

                if(shareDPreferrence.deleteSharedData()) {
                    Intent intent = new Intent(ClarkeAfterLoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                break;


            case R.id.changeEmailId :
                Toast.makeText(this, "Clicked Change Email Menu", Toast.LENGTH_SHORT).show();
                break;

            case R.id.changePasswordId :
                Toast.makeText(this, "Clicked Change Password Menu", Toast.LENGTH_SHORT).show();
                break;


        }
        return super.onOptionsItemSelected(item);
    }
//**********************************End Menu Item Work**********************************************


}
