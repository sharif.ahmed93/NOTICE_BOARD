package com.example.sharifahmed.nstu_nb2.api;




import com.example.sharifahmed.nstu_nb2.ResponseFromServer.NOTICEJSONDataResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    /*
    Retrofit get annotation with our URL
    And our method that will return us the List of ContactList
    */

    //@GET("/json_data.json")
    //Call<ContactList> getMyJSON();

    //@GET("AndroidPhpMysqlLoginRegister/allusers.php")
    //Call<List<NOTICEJSONDataResponse>> getMyJSON();

    @GET("NSTU_NOTICE_BOARD/students_notice.php")
    Call<List<NOTICEJSONDataResponse>> getMyJSON();
}
