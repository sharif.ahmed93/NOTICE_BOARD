package com.example.sharifahmed.nstu_nb2.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sharifahmed.nstu_nb2.R;
import com.example.sharifahmed.nstu_nb2.ResponseFromServer.NoticeResponse;

import java.util.List;

/**
 * Created by SHARIF AHMED on 12/15/2016.
 */
public class NoticeAdapter extends ArrayAdapter<NoticeResponse> {
    private Context context;
    private List<NoticeResponse> noticeList;


    private class ViewHolder {
        TextView noticeTitleTextView;
        TextView viewDetailsTextView;
        LinearLayout linearLayout;
    }

    public NoticeAdapter(Context context, List<NoticeResponse> noticeList) {
        super(context, R.layout.custom_all_students_notice_list_layout, noticeList);
        this.context = context;
        this.noticeList = noticeList;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.custom_all_students_notice_list_layout, null, true);
            viewHolder = new ViewHolder();

            viewHolder.noticeTitleTextView = (TextView) convertView.findViewById(R.id.noticeTitleTextId);
            viewHolder.viewDetailsTextView = (TextView) convertView.findViewById(R.id.viewDetailsText);
            viewHolder.linearLayout = (LinearLayout) convertView.findViewById(R.id.customLinearid);

            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) convertView.getTag();

        }

        viewHolder.noticeTitleTextView.setText(noticeList.get(position).getTitle());

        /*
        viewHolder.viewDetailsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SavedValue.id = userList.get(position).getId();
                Log.e("Click id : ",userList.get(position).getId());
                SavedValue.full_name= userList.get(position).getFullName();
                SavedValue.user_name = userList.get(position).getUserName();
                SavedValue.phone_number = userList.get(position).getPhoneNumber();
                SavedValue.email = userList.get(position).getEmail();

                String id = userList.get(position).getId();
                Intent intent = new Intent(context.getApplicationContext(),ViewDetailsActivity.class);
                intent.putExtra("UserId",id);
                context.startActivity(intent);
            }
        });
*/
        return convertView;
    }
}
